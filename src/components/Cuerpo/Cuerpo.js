import React from 'react';
import Grid from '@material-ui/core/Grid';
import './Cuerpo.css';
import Aprende from '../img/aprender-a-programar.jpg';
import Desarrollo from '../img/desarrollo.jpg';
import Disenio from '../img/diseniador.jpg';

export default function Cuerpo(){
  return(
    <div className="cuerpo">
      <div className="intro">
        <div className="flex">
        <div className="flex areas">
          Areas de conocimiento
        </div>
        <div className="text_areas">
          Nuestra oferta académica es muy diversa e incluye desde cursos básicos e introductorios hasta aquellos de alto nivel de especialización. Las sesiones tienen un enfoque práctico para apoyar a los participantes lograr un completo dominio de las temáticas. A quienes aprueban los cursos se les entrega constancia con registro 
        </div>
       </div>
       <div className="numeros">
          <Grid container>
            <Grid item md={4}>
              <div className="relativo bases">
                <div className="abs numero_titulo">
                  Bases de datos
                </div>
                <div className="numero">01</div>
                <div className="text_titulo">
                  Lorem ipsum dolor sit amet consectetur
                </div>
                <div className="relativo imagen">
                  <img className="relativo" src={Aprende} alt="d"/>
                </div>
              </div>
            </Grid>
            <Grid item md={4}>
            <div className="relativo desarrollo">
                <div className="abs numero_titulo">
                  Desarrollo de aplicaciones <br/>para moviles 
                </div>
                <div className="numero">02</div>
                <div className="text_titulo">
                  Lorem ipsum dolor sit amet consectetur
                </div>
                <div className="relativo imagen_des">
                  <img className="relativo" src={Desarrollo} alt="d"/>
                </div>
              </div>

            </Grid>
            <Grid item md={4}>
              <div className="relativo disenio">
                <div className="abs numero_titulo">
                  Diseño editorial y grafico 
                </div>
                <div className="numero">03</div>
                <div className="text_titulo">
                  Lorem ipsum dolor sit amet consectetur
                </div>
                <div className="relativo imagen_dis">
                  <img className="relativo" src={Disenio} alt="d"/>
                </div>
              </div>
            </Grid>
          </Grid>
        </div> 
      </div>
    </div>
  );
}