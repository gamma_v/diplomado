import React from 'react';
import './App.css';
import Carrusel from './components/Carrusel/';
import Menu from './components/Menu/';
import Cuerpo from './components/Cuerpo/';
import Footer from './components/Footer/';

function App() {
  return (
    <div>
      <Menu />
      <Carrusel />
      <Cuerpo />
      <Footer />
    </div>
  );
}

export default App;
